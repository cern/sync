# sync

Ultra sophisticated sync software used in the Large Hadron Collider Kickers, required to be extremely synchronized, difference in times varies from 3 to 5 Planck times.